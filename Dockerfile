###############################################################################
# Hestio Container - MkDocs
###############################################################################
FROM squidfunk/mkdocs-material

###############################################################################
# ARGs
###############################################################################
ARG HTTP_PROXY="${http_proxy}"
ARG http_proxy="${http_proxy}"
ARG HTTPS_PROXY="${https_proxy}"
ARG https_proxy="${https_proxy}"
ARG no_proxy="${no_proxy}"
ARG NO_PROXY="${NO_PROXY}"

ARG BRANCH
ARG VERSION
ARG IMAGE
ARG GIT_URL

###############################################################################
# ENVs
###############################################################################
ENV APPDIR="/docrepo"
ENV DATADIR="/data"
ENV CONFDIR="/conf"
ENV PUID 1000
ENV PGID 1000

###############################################################################
# LABELs
###############################################################################

WORKDIR ${APPDIR}

COPY . ${APPDIR}

RUN \
    # install Python dependencies
    python -m pip install -r requirements/requirements.py2 --upgrade --disable-pip-version-check && \
    # cleanup after installations
    rm -rf /var/cache/apk/*

EXPOSE 8080

ENTRYPOINT ["mkdocs"]
CMD ["serve", "--no-livereload", "--dev-addr", "0.0.0.0:8080"]
