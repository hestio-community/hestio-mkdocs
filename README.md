# Hestio MkDocs

MkDocs + Material + Mermaid2 diagram support - all ready to use in a Docker container!

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```

## Status

[![pipeline status](https://gitlab.com/hestio-community/hestio-mkdocs/badges/master/pipeline.svg)](https://gitlab.com/hestio-community/hestio-mkdocs/-/commits/master)


## Get and use our Hestio MkDocs container

**Basic Usage with your own MkDocs content**

```bash
docker run -it -v "$(pwd):/docrepo" hestio/mkdocs --help
```

```bash
docker run -it -v "$(pwd):/docrepo" -p 8080:8080 hestio/mkdocs serve
```

**Provide your own MkDocs config**

```bash
docker run -it \
    -v "$(pwd):/docrepo" \
    -v "$(pwd)/mkdocs.yml:/docs/mkdocs.yml" \
    -p 8080:8080 \
    hestio/mkdocs
```

**Build static site content using your own MkDocs config**

```bash
docker run -it \
    -v "$(pwd):/docrepo" \
    -v "static-site:/site" \
    -v "$(pwd)/mkdocs.yml:/docs/mkdocs.yml" \
    hestio/mkdocs build --site-dir /site
```
